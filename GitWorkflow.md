# GitPaid Git Workflow

![Git Workflow](GitWorkflow.png)

## Branches

### `master` branch

This branch is for our most stable code. You should never commit directly to the `master` branch. At the end of each sprint, the `develop` branch will be merged into the `master` branch.

### `develop` branch

This is essentially the sprint branch. It branches from `master` and contains the code for the current sprint. You should never commit directly to the `develop` branch. You will merge your code into the `develop` branch by opening a merge request in GitLab and adding other team members as reviewers. Once your reviewers approve the merge request, your code will be merged into `develop`.

### `GP-#/feature`

When you are working on a task for the sprint, checkout a new branch called `GP-#/feature` from `develop` where `#` is the Jira issue number and `feature` is a short description of the task. Push all you commits to this branch. Once you are done with the task, open a merge request on GitLab to merge your branch into the `develop` branch.

## Common Workflow

This is an example of a common workflow when you are working on a task for a sprint. Example: you are assigned to complete Jira issue GP-5 that deals with password resetting.

Before doing anything, make sure that you have a clean working directory with no changes.

### Pull the latest changes from `develop`

> `git checkout develop` //checkout the develop branches
>
> `git pull` //Pull changes from develop so your branch is up to date

You now are on the latest version of the `develop` branch.

### Checkout a new feature branch for

> `git checkout -b GP-5/password-resetting`

You are now on your feature branch and can commit code to this branch.

### Committing code to your branch

> //do your work
>
> `git add .` //add your files to the staging directory
>
> `git commit -m 'GP-5 short but descriptive message'` //add your commit message
>
> `git push` //push your changes to the remote repo

Your changes are now on the remote repo on GitLab

### Opening a merge request

Once you are done committing all your work you'll need to open a merge request in GitLab to merge your branch into the `develop` branch. Before you do that, you will need to pull in any changes that may have happened on the `develop` branch since you last checked it out. This will help avoid merge conflicts.

> `git checkout develop` // checkout develop branch
>
> `git pull` // pull the latest changes
>
> //if there are any changes you must merge develop with your branch
>
> `git checkout GP-5/password-resetting` // checkout your branches
>
> `git merge develop` //merge the develop branch into your branches
>
> //If there are merge conflicts resolve them
>
> `git push` //push the changes to your branch

Now your branch should be up to date with all the latest changes and you can open a merge request.

In GitLab, select the Merge Request button on the side. Click 'New Merge Request'. Select your branch that you committed your work on as the source branch and the `develop` branch as the target branch. Click 'Compare branches and continue'. You should be on the New Merge Request screen. This is where you'll enter all the info for you merge request.

The `Title` should start with the Jira issue and be short but descriptive. For example, 'GP-5 Add Password Resetting'. Be sure to start you title with the Jira issue number. In the description box, describe the changes you made, why you made them, and anything in specific you would like the reviewers to look at. Select yourself as the assignee. For reviewers, select at least 3 people to review your code. If you would like to add everyone that's fine. Be sure to add people who need to know of your changes as reviewers. Everything else can be left as is and you can click 'Submit merge request'.

### Merging a merge Request

Once at least 3 people have approved your merge request, you can merge your merge request
